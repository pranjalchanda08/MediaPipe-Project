# Mediapipe Projects
A set of project using OpenCV and Mediapipe library.

## System Requirements
* Python 3.2 - 3.7
* **No GPU** requirements

## Installation
Typically a pycharm based project. 

```sh
pip3 install -r requirements.txt
```
## Utilities
- [X] Hand Tracking
- [ ] Pose Estimation
- [ ] Face Detection

## Reference
[Mediapipe](https://google.github.io/mediapipe/)
[OpenCV](https://pypi.org/project/opencv-python/)
